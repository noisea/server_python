### fazer repositório gitlab
> criar server em pasta isolada para testar projetos python
> criar pasta em diretório

`mkdir sanbox`

>ir para pasta

`cd sandbox/`
`mkdir pastaprojetos`
`cd bot/`

>ativar python

`py`

>ativar server

`python -m venv _venv`
`ls`

>ativar ou desativar server 

`. _venv/bin/activate`
`deactivate`
`._venv/bin/activate`

>fazer upgrade ao pip que é o que permite instalar programas em python

`pip install pip --upgrade`

>instalar prog python

`pip install ______`

>criar file em determinada extensão no vacodium, por exemplo um ficheiro yaml

`vscodium config.yaml`

---
>teste de projeto para bot chatgpt no telegram - https://pypi.org/project/chatgpt-telegram/
```
{
cd sandbox/bot/
. _venv/bin/activate
vscodium config.yaml
pip install websockets
playwright install
python -m chatgpt_telegram config.yaml
config.yaml
telegram_api_key: "________"
accounts:
- user: "___________"
`psw: "__________"
```
{